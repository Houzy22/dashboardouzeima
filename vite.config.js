import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  root: 'src',
  build: {
    rollupOptions: {
      input: 'App.jsx'
    },
    // Configuration spécifique pour le déploiement Vercel
    command: 'vite build --config vite.config.js'
  }
})